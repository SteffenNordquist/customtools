
//creator of all elements that appear on an amazon page
//checking if we are in the right amazon page to work with
if(document.title == "Seller Central: Käufer-Verkäufer-Postfach - Amazon Seller Central"){
	var container = document.getElementsByClassName('cols-right')[0];

		var division = document.createElement("div");
		division.setAttribute("class", "cols-r1");
		division.id = "customerServiceToolContainer"
			
			//customer tool container label
			var labelDivision = document.createElement("div");
			labelDivision.setAttribute("class", "righttitle");	
				var labelDivisionText = document.createTextNode("Customer Service Tool");
				labelDivision.appendChild(labelDivisionText)
			division.appendChild(labelDivision);
			
			var optionsDivision = document.createElement("a");
			optionsDivision.setAttribute("style", "float:right;");	
			optionsDivision.setAttribute("target", "_blank");	
				var optionsDivisionText = document.createTextNode("Options");
				optionsDivision.appendChild(optionsDivisionText)
			var optionsUrl = chrome.extension.getURL("options.html"); 
			optionsDivision.href = optionsUrl;
			labelDivision.appendChild(optionsDivision);

			//purchased date division
			var purchasedDateDivision = document.createElement("div");
			purchasedDateDivision.setAttribute("style", "line-height:1.4em;");
			
				var purchasedValueDivision = document.createElement("div");
				purchasedValueDivision.setAttribute("style", "float:right; padding: 5px; background-color: rgb(239, 239, 239);"); 
				purchasedValueDivision.id = "PurchasedDateHere";
					var purchasedDateValueText = document.createTextNode("PurchasedDate Here");
					purchasedValueDivision.appendChild(purchasedDateValueText);
					
				purchasedDateDivision.appendChild(purchasedValueDivision);
				
				var purchasedDateLabelDivision = document.createElement("div");
				purchasedDateLabelDivision.setAttribute("style", "padding:5px 0 5px 0"); 
					var purchasedDateLabelText = document.createTextNode("Bestelldatum");
					purchasedDateLabelDivision.appendChild(purchasedDateLabelText);
					
				purchasedDateDivision.appendChild(purchasedDateLabelDivision);
				
				var purchasedDateClearDivision = document.createElement("div");
				purchasedDateClearDivision.setAttribute("style", "clear: both; padding: 0;"); 
					
				purchasedDateDivision.appendChild(purchasedDateClearDivision);
				
			division.appendChild(purchasedDateDivision);

			//order date division
			var orderDateDivision = document.createElement("div");
			orderDateDivision.setAttribute("style", "line-height:1.4em;");
			
				var orderDateValueDivision = document.createElement("div");
				orderDateValueDivision.setAttribute("style", "float:right; padding: 5px; background-color: rgb(239, 239, 239);"); 
				orderDateValueDivision.id = "OrderDateHere";
					var orderDateValueText = document.createTextNode("OrderDate Here");
					orderDateValueDivision.appendChild(orderDateValueText);
					
				orderDateDivision.appendChild(orderDateValueDivision);
				
				var orderDateLabelDivision = document.createElement("div");
				orderDateLabelDivision.setAttribute("style", "padding:5px 0 5px 0"); 
					var orderDateLabelText = document.createTextNode("Lieferantenbestellung");
					orderDateLabelDivision.appendChild(orderDateLabelText);
					
				orderDateDivision.appendChild(orderDateLabelDivision);
				
				var orderDateClearDivision = document.createElement("div");
				orderDateClearDivision.setAttribute("style", "clear: both; padding: 0;"); 
					
				orderDateDivision.appendChild(orderDateClearDivision);
				
			division.appendChild(orderDateDivision);

			//shipping date division
			var shipDateDivision = document.createElement("div");
			shipDateDivision.setAttribute("style", "line-height:1.4em;");
			
				var shipDateValueDivision = document.createElement("div");
				shipDateValueDivision.setAttribute("style", "float:right; padding: 5px; background-color: rgb(239, 239, 239);"); 
				shipDateValueDivision.id = "ShipDateValueHere";
					var labelText = document.createTextNode("shipping date here");
					shipDateValueDivision.appendChild(labelText);
					
				shipDateDivision.appendChild(shipDateValueDivision);
				
				var shipDateLabelDivision = document.createElement("div");
				shipDateLabelDivision.setAttribute("style", "padding:5px 0 5px 0"); 
					var valueText = document.createTextNode("Versendet");
					shipDateLabelDivision.appendChild(valueText);
					
				shipDateDivision.appendChild(shipDateLabelDivision);
				
				var shipDateClearDivision = document.createElement("div");
				shipDateClearDivision.setAttribute("style", "clear: both; padding: 0;"); 
					
				shipDateDivision.appendChild(shipDateClearDivision);
				
			division.appendChild(shipDateDivision);

			//current stock division
			var currentStockDivision = document.createElement("div");
			currentStockDivision.setAttribute("style", "line-height:1.4em;");
			
				var currentStockValueDivision = document.createElement("div");
				currentStockValueDivision.setAttribute("style", "float:right; padding: 5px; background-color: rgb(239, 239, 239);"); 
				currentStockValueDivision.id = "StockValueHere";
					var currentStockValueText = document.createTextNode("current stock here");
					currentStockValueDivision.appendChild(currentStockValueText);
					
				currentStockDivision.appendChild(currentStockValueDivision);
				
				var currentStockLabelDivision = document.createElement("div");
				currentStockLabelDivision.setAttribute("style", "padding:5px 0 5px 0"); 
					var currentStockLabelText = document.createTextNode("Lager");
					currentStockLabelDivision.appendChild(currentStockLabelText);
					
				currentStockDivision.appendChild(currentStockLabelDivision);
				
				var curStockClearDivision = document.createElement("div");
				curStockClearDivision.setAttribute("style", "clear: both; padding: 0;"); 
					
				currentStockDivision.appendChild(curStockClearDivision);
				
			division.appendChild(currentStockDivision);

			

			//supplier availability division
			var availabilityDivision = document.createElement("div");
			availabilityDivision.setAttribute("style", "line-height:1.4em;");
			
				var availabilityValueDivision = document.createElement("div");
				availabilityValueDivision.setAttribute("style", "float:right; padding: 5px; background-color: rgb(239, 239, 239);"); 
				availabilityValueDivision.id = "SupplierAvailabilityHere";
					var availabilityValueText = document.createTextNode("SupplierAvailabilityHere");
					availabilityValueDivision.appendChild(availabilityValueText);
					
				availabilityDivision.appendChild(availabilityValueDivision);
				
				var availabilityLabelDivision = document.createElement("div");
				availabilityLabelDivision.setAttribute("style", "padding:5px 0 5px 0"); 
					var availabilityLabelText = document.createTextNode("Availability");
					availabilityLabelDivision.appendChild(availabilityLabelText);
					
				availabilityDivision.appendChild(availabilityLabelDivision);
				
				var availabilityClearDivision = document.createElement("div");
				availabilityClearDivision.setAttribute("style", "clear: both; padding: 0;"); 
					
				availabilityDivision.appendChild(availabilityClearDivision);
				
			division.appendChild(availabilityDivision);
			
			//availability date division
			var availabilityDateDivision = document.createElement("div");
			availabilityDateDivision.setAttribute("style", "line-height:1.4em;");
			
				var availabilityDateValueDivision = document.createElement("div");
				availabilityDateValueDivision.setAttribute("style", "float:right; padding: 5px; background-color: rgb(239, 239, 239);"); 
				availabilityDateValueDivision.id = "AvailabilityDateHere";
					var availabilityDateValueText = document.createTextNode("AvailabilityDateHere");
					availabilityDateValueDivision.appendChild(availabilityDateValueText);
					
				availabilityDateDivision.appendChild(availabilityDateValueDivision);
				
				var availabilityDateLabelDivision = document.createElement("div");
				availabilityDateLabelDivision.setAttribute("style", "padding:5px 0 5px 0"); 
					var availabilityDateLabelText = document.createTextNode("Availability Date");
					availabilityDateLabelDivision.appendChild(availabilityDateLabelText);
					
				availabilityDateDivision.appendChild(availabilityDateLabelDivision);
				
				var availabilityDateClearDivision = document.createElement("div");
				availabilityDateClearDivision.setAttribute("style", "clear: both; padding: 0;"); 
					
				availabilityDateDivision.appendChild(availabilityDateClearDivision);
				
			division.appendChild(availabilityDateDivision);
			
			//reOrderDate division
			var reOrderDateDivision = document.createElement("div");
			reOrderDateDivision.setAttribute("style", "line-height:1.4em;");
			
				var reOrderDateValueDivision = document.createElement("div");
				reOrderDateValueDivision.setAttribute("style", "float:right; padding: 5px; background-color: rgb(239, 239, 239);"); 
				reOrderDateValueDivision.id = "ReOrderDateHere";
					var reOrderDateValueText = document.createTextNode("ReOrderDateHere");
					reOrderDateValueDivision.appendChild(reOrderDateValueText);
					
				reOrderDateDivision.appendChild(reOrderDateValueDivision);
				
				var reOrderDateLabelDivision = document.createElement("div");
				reOrderDateLabelDivision.setAttribute("style", "padding:5px 0 5px 0"); 
					var reOrderDateLabelText = document.createTextNode("ReOrderDate");
					reOrderDateLabelDivision.appendChild(reOrderDateLabelText);
					
				reOrderDateDivision.appendChild(reOrderDateLabelDivision);
				
				var reOrderDateClearDivision = document.createElement("div");
				reOrderDateClearDivision.setAttribute("style", "clear: both; padding: 0;"); 
					
				reOrderDateDivision.appendChild(reOrderDateClearDivision);
				
			division.appendChild(reOrderDateDivision);
			
			//reOrder button division
			var reOrderButtonDivision = document.createElement("div");
			reOrderButtonDivision.setAttribute("style","width:95%;height:55px;padding-top:5px;padding-bottom:5px;");
			reOrderButtonDivision.id = 'reOrderButtonDivision';
				var reOrderButton = document.createElement("button");
				reOrderButton.id = "reOrderButton";
				reOrderButton.setAttribute("style","padding:0;margin:0;width:100%;height:55px;background-color:#34a0c8;");
					var buttonValue = document.createTextNode("Reorder");
					reOrderButton.appendChild(buttonValue);
			reOrderButtonDivision.appendChild(reOrderButton);
				
			division.appendChild(reOrderButtonDivision);

			//storno button division
			var stornoButtonDivision = document.createElement("div");
			stornoButtonDivision.setAttribute("style","width:95%;height:55px;padding-top:0px;padding-bottom:5px;");
			stornoButtonDivision.id = "stornoButtonDivision";
				var stornoButton = document.createElement("button");
				stornoButton.id = "stornoButton";
				stornoButton.setAttribute("style","padding:0;margin:0;width:100%;height:55px;background-color:#34a0c8;");
					var stornoValue = document.createTextNode("Add to storno");
					stornoButton.appendChild(stornoValue);
			stornoButtonDivision.appendChild(stornoButton);
				
			division.appendChild(stornoButtonDivision);

			//skublocking button division
			var skuBlockingButtonDivision = document.createElement("div");
			skuBlockingButtonDivision.setAttribute("style","width:95%;height:55px;padding-top:0px;");
			skuBlockingButtonDivision.id = "skuBlockButtonDivision";
				var skuBlockingButton = document.createElement("button");
				skuBlockingButton.id = "skuBlockButton";
				skuBlockingButton.setAttribute("style","padding:0;margin:0;width:100%;height:55px;background-color:#34a0c8;");
					var skuBlockingbuttonValue = document.createTextNode("Block");
					skuBlockingButton.appendChild(skuBlockingbuttonValue);
			skuBlockingButtonDivision.appendChild(skuBlockingButton);
				
			division.appendChild(skuBlockingButtonDivision);
			
			//set product stock button division
			var setProductStockButtonDivision = document.createElement("div");
			setProductStockButtonDivision.setAttribute("style","width:95%;height:55px;padding-top:0px;");
			setProductStockButtonDivision.id = "setProductStockButtonDivision";
				var setProductStockButton = document.createElement("button");
				setProductStockButton.id = "skuBlockButton";
				setProductStockButton.setAttribute("style","padding:0;margin:0;width:100%;height:55px;background-color:#34a0c8;");
					var setProductStockButtonValue = document.createTextNode("Set Stock To 0");
					setProductStockButton.appendChild(setProductStockButtonValue);
			setProductStockButtonDivision.appendChild(setProductStockButton);
				
			division.appendChild(setProductStockButtonDivision);

	container.appendChild(division);



			//floating button
			//var GoToButtonDivision = document.createElement("div");
			//GoToButtonDivision.setAttribute("style","width:100px;height:55px;position:fixed;");
			//GoToButtonDivision.id = "GoToButtonDivision";
				var GoToButton = document.createElement("button");
				GoToButton.id = "GoToButton";
				GoToButton.setAttribute("style","width:150px;height:55px;background-color:#34a0c8;border-radius:5px;float:right;margin-bottom:5px;");
					var GoTobuttonValue = document.createTextNode("Go To Reply Buttons");
					GoToButton.appendChild(GoTobuttonValue);
			//GoToButtonDivision.appendChild(GoToButton);
			
			document.getElementById('sc-masthead').appendChild(GoToButton);
			document.getElementById('GoToButton').addEventListener('click',
				function(){
					//document.getElementsByClassName("CustomReplyButtons")[0].focus();
					window.location.hash = '#CustomReplyButtons';
					
				}
			);

}

//checking if we are in the right amazon page to work with
if(
	document.title == "Seller Central: Käufer-Verkäufer-Postfach - Amazon Seller Central"
	&& 
	document.getElementsByClassName("titletab-active")[0].innerText == "Empfangene Nachrichten"
  )
{
	ScrapeMessages();
}


function ScrapeMessages(){
	var messageContainers = document.getElementsByClassName("list-row-white");
	
	var messageRows = [];
	
	if(messageContainers != null && messageContainers != undefined){
		for(var i=0; i<messageContainers.length; i++)
		{
			var tds = messageContainers[i].children;
			var messageLink = tds[3].getElementsByTagName("a")[0].attributes["href"].value;
			var arrivalDate = messageLink.split('arrivalDate=')[1].split('&')[0];
			
			messageRows.push(
				{
					"arrivalDate" : arrivalDate,
					"amazonOrderId" : tds[5].innerText,
					"amazonOrderIdContainer" : tds[5]
				}
			);
		}
	}
	
	messageRows.forEach(function(m){
		SaveMessage(m.arrivalDate, m.amazonOrderId, m.amazonOrderIdContainer, messageRows);
	});
}

function SaveMessage(arrivalDate, amazonOrderId, amazonOrderIdContainer, messageRows){
	//var wcfUrl = 'http://localhost:1161/AmazonCustomerMsgService.svc/SaveMessage?arrivalDate=' + arrivalDate + '&amazonOrderId=' + amazonOrderId ;
	var wcfUrl = GetWcfServiceUrl() + '/AmazonCustomerMsgService.svc/SaveMessage?arrivalDate=' + arrivalDate + '&amazonOrderId=' + amazonOrderId ;
		$.ajax({
                    type: "GET",
                    url: wcfUrl,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    processData: true,
                    success: function(result) {
                    	console.log(result);
						for(var i=0; i<messageRows.length; i++)
						{
							if(messageRows[i].amazonOrderId == amazonOrderId){
								DisplayMessageCount(messageRows[i].amazonOrderId, messageRows[i].amazonOrderIdContainer);
							}
						}
						//if(result.toString().indexOf("success") != -1){
						//	amazonOrderIdContainer.innerText = result.split('|')[0] + amazonOrderId
						//}
						/* if(result.split('|')[0] != "0"){
							amazonOrderIdContainer.getElementsByTagName("a")[0].innerText = "(" + result.split('|')[0].replace("\"","") + ")" + amazonOrderId;
							amazonOrderIdContainer.setAttribute("class","");
							amazonOrderIdContainer.style.width = "180px";
							amazonOrderIdContainer.style.fontSize = "11px";
							amazonOrderIdContainer.style.fontFamily = "verdana,arial,helvetica,sans-serif;";
						} */
                    },
                    error: function(xhr, status, error) {
					  var err = JSON.parse(xhr.responseText);
					  alert(err.Message);
					}
                });
}

function DisplayMessageCount(amazonOrderId, amazonOrderIdContainer){
	//var wcfUrl = 'http://localhost:1161/AmazonCustomerMsgService.svc/MessageCount?amazonOrderId=' + amazonOrderId ;
	var wcfUrl = GetWcfServiceUrl() + '/AmazonCustomerMsgService.svc/MessageCount?amazonOrderId=' + amazonOrderId ;
		$.ajax({
                    type: "GET",
                    url: wcfUrl,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    processData: true,
                    success: function(response) {
						
							amazonOrderIdContainer.getElementsByTagName("a")[0].innerText = "(" + response + ")" + amazonOrderId;
							amazonOrderIdContainer.setAttribute("class","");
							amazonOrderIdContainer.style.width = "180px";
							amazonOrderIdContainer.style.fontSize = "11px";
							amazonOrderIdContainer.style.fontFamily = "verdana,arial,helvetica,sans-serif;";
						
                    },
                    error: function(xhr, status, error) {
					  var err = JSON.parse(xhr.responseText);
					  alert(err.Message);
					}
                });
}
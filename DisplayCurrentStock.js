
//use for diplaying the stock of the amazon order id
if(document.title == "Seller Central: Käufer-Verkäufer-Postfach - Amazon Seller Central"){
		var amazonOrderId = GetAmazonOrderId();//this function is located in OrderIdParser.js
		if(amazonOrderId != ""){
			//wcf url that will return a stock value of the amazon order id
			var wcfUrl = GetWcfServiceUrl() + "/CurrentStockService.svc/GetStock/" + amazonOrderId;
			//invokes the function located in ExternalServices.js
			InvokeGetServiceAndDisplay(wcfUrl, "StockValueHere");
		}
}
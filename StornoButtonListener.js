
//checking if we are in the right amazon page to work with
if(document.title == "Seller Central: Käufer-Verkäufer-Postfach - Amazon Seller Central"){
	SetupStorno();
}

function SetupStorno(){
	var amazonOrderId = GetAmazonOrderId();
	AjaxCheckToCollection(amazonOrderId);
}

//currently not in use
function CheckToCollection(amazonOrderId){
		var x = new XMLHttpRequest();
		x.open('GET', GetWcfServiceUrl() + '/StornoCollectionService.svc/HasAdded/' + amazonOrderId);//web service link
		x.onload = function() {
			var hasAdded = JSON.parse(x.responseText);
			if(hasAdded==false || hasAdded == "false"){
				document.getElementById('stornoButton').addEventListener('click', AddToCollection);
			}
			else if(hasAdded==true || hasAdded == "true"){
				HideButton('stornoButtonDivision');
			}


		};
		x.send();
}

//checks if the amazon order id already added to storno collection
//if exists 'add to storno' button will be hidden
//else adds an event listener for 'add to storno' button
function AjaxCheckToCollection(amazonOrderId){
		var wcfUrl = GetWcfServiceUrl() + '/StornoCollectionService.svc/HasAdded/'  + amazonOrderId;
		$.ajax({
                    type: "GET",
                    url: wcfUrl,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processData: true,
                    success: function(result) {
                    	if(result==false || result == "false"){
							document.getElementById('stornoButton').addEventListener('click', AddToCollection);
						}
						else if(result==true || result == "true"){
							HideButton('stornoButtonDivision');
						}
                    },
                    error:  function(result) {
	                    console.log(result);
	                }
                });
}

//executes adding to storno collection operation
function AddToCollection(){
		var amazonOrderId = GetAmazonOrderId();
		var x = new XMLHttpRequest();
		x.open('GET', GetWcfServiceUrl() + '/StornoCollectionService.svc/Add/' + amazonOrderId);//web service link
		x.onload = function() {
			var addResponse = JSON.parse(x.responseText);
			
			if (addResponse.indexOf("ok") >= 0) {
        		HideButton('stornoButtonDivision');
    		}

		};
		x.send();
}

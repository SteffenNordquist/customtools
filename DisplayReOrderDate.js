
//checking if we are in the right amazon page to work with
if(document.title == "Seller Central: Käufer-Verkäufer-Postfach - Amazon Seller Central")
{
		var amazonOrderId = GetAmazonOrderId();//function located in OrderIdParser.js
		if(amazonOrderId != ""){
			//wcf service that will return cheapest supplier information of the amazon order id
			var wcfUrl = GetWcfServiceUrl() + "/ReOrderDateService.svc/GetReOrderDate?amazonOrderId=" + amazonOrderId;
			//invokes the function located in ExternalServices.js
			InvokeGetServiceAndDisplay(wcfUrl, "ReOrderDateHere");
		}
}
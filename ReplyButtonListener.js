
function SetupReply(messageName){
	
	var amazonOrderId = GetAmazonOrderId();
		
	AjaxGetCustomMessage(amazonOrderId,messageName);
}

function GetEmail(){
	var emailContainer = document.getElementsByClassName('tiny')[1];
		
	var amazonEmail = emailContainer.innerHTML.replace("(","").replace(")","");

	return amazonEmail;
}

//not in use currently
function GetCustomMessage(amazonOrderId,messageName){
		//amazonEmail = "berliner.versandwerk@gmail.com";
		var x = new XMLHttpRequest();
		//x.open('GET', GetWcfServiceUrl() + '/AgentCustomReplyMsgService.svc/GetAgentCustomMessage/' +amazonOrderId+ '/' + messageName);//web service link
		x.open('Get', GetWcfServiceUrl() + '/AgentCustomReplyMsgService.svc/GetAgentCustomMessage?amazonOrderId=' +amazonOrderId+ '&shortName=' + messageName);
		x.onload = function() {
			var getCustomMessageResponse = JSON.parse(x.responseText);
			
			if(getCustomMessageResponse != ""){
				InputReplyMessage(getCustomMessageResponse);
			}
			else{
				alert(messageName + " : dont have custom message");
			}
		};
		x.send();
}

//gets custom message with supplierd amazon order id and message name
function AjaxGetCustomMessage(amazonOrderId,messageName){
		//var wcfUrl = GetWcfServiceUrl() + '/AgentCustomReplyMsgService.svc/GetAgentCustomMessage/' +amazonOrderId+ '/' + messageName;
		var wcfUrl = GetWcfServiceUrl() + '/AgentCustomReplyMsgService.svc/GetAgentCustomMessage?amazonOrderId=' +amazonOrderId+ '&shortName=' + messageName;
		$.ajax({
                    type: "GET",
                    url: wcfUrl,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processData: true,
                    success: function(result) {
						if(result != ""){
							InputReplyMessage(result);
						}
                    },
                    error:  function(result) {
	                    console.log(result);
	                }
                });
}

//inputs the reply message in the amazon page textbox
function InputReplyMessage(customMessage){
	var textBoxNode = document.getElementById('commMgrCompositionMessage');
	
	if(textBoxNode != null){
		//textBoxNode.innerHTML = customMessage;
		textBoxNode.value = customMessage;
		window.location.hash = '#commMgrCompositionMessage';
		document.getElementById("ReplyButton").focus();
		console.log("ReplyButton focused");
	}
}

//checking if we are in the right amazon page to work with
if(document.title == "Seller Central: Käufer-Verkäufer-Postfach - Amazon Seller Central")
{
	AjaxHasBlocked();
	AjaxSkuIsEanNullEmpty();
}

//not in used currently
function HasBlocked(){
		var amazonOrderId = GetAmazonOrderId();
		var wcfUrl = GetWcfServiceUrl() + '/SkuBlockingService.svc/HasBlockedSku/' + amazonOrderId;

		var x = new XMLHttpRequest();
		x.open('GET', wcfUrl);
		x.onload = function() {
			var hasBlocked = x.responseText;
			
			if(hasBlocked == false || hasBlocked == "false"){
				document.getElementById('skuBlockButton').addEventListener('click', SetupSkuBlocking);
			}
			else if(hasBlocked == true || hasBlocked == "true"){
				HideButton('skuBlockButtonDivision');
			}
		};
		x.send();
}

//checks if the amazon order id's sku is blocked
//once it is not blocked, adds event listener for 'block sku' button, else 'block sku' will be hidden
function AjaxHasBlocked(){
	var amazonOrderId = GetAmazonOrderId();
	var wcfUrl = GetWcfServiceUrl() + '/SkuBlockingService.svc/HasBlockedSku/' + amazonOrderId;
	$.ajax({
                    type: "GET",
                    url: wcfUrl,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processData: true,
                    success: function(result) {
						if(result == false || result == "false"){
							document.getElementById('skuBlockButton').addEventListener('click', SetupSkuBlocking);
						}
						else if(result == true || result == "true"){
							HideButton('skuBlockButtonDivision');
						}
                        
                    },
                    error:  function(result) {
	                    console.log(result);
	                }
                });
}

//currently not in use
function SkuIsEanNullEmpty(){
		var amazonOrderId = GetAmazonOrderId();
		var wcfUrl = GetWcfServiceUrl() + '/SkuBlockingService.svc/SkuIsEanOrEmptyOrNull/' + amazonOrderId;

		var x = new XMLHttpRequest();
		x.open('GET', wcfUrl);
		x.onload = function() {
			var isEanNullEmpty = x.responseText;
			if(isEanNullEmpty == true || isEanNullEmpty == "true"){
				HideButton('skuBlockButtonDivision');
			}
		};
		x.send();

		
}

//checks if sku is null or empty
function AjaxSkuIsEanNullEmpty(){
	var amazonOrderId = GetAmazonOrderId();
	var wcfUrl = GetWcfServiceUrl() + '/SkuBlockingService.svc/SkuIsEanOrEmptyOrNull/' + amazonOrderId;
	$.ajax({
                    type: "GET",
                    url: wcfUrl,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processData: true,
                    success: function(result) {
						if(result == true || result == "true"){
							HideButton('skuBlockButtonDivision');
						}
                        
                    },
                    error:  function(result) {
	                    console.log(result);
	                }
                });
}

//triggered once 'block sku' button is being clicked
function SetupSkuBlocking(){

	var amazonOrderId = GetAmazonOrderId();

	if(amazonOrderId != ""){
		if (confirm('This process cant be undone. Are you sure you want to block this item?')) 
		{
				//SkuBlockingExecute(amazonOrderId);
				AjaxSkuBlockingExecute(amazonOrderId);
		}
	}
	else{
		alert("AmazonOrderId not found!");
	}	
}

//currently not in used
function SkuBlockingExecute(amazonOrderId){
		var x = new XMLHttpRequest();
		x.open('GET', GetWcfServiceUrl() + '/SkuBlockingService.svc/BlockSku/' + amazonOrderId);//web service link
		x.onload = function() {
			var skuBlockingResponse = x.responseText;
			alert(skuBlockingResponse);
			if(skuBlockingResponse == "Blocking Operation succeeded"){
				HideButton('skuBlockButtonDivision');
			}	
		};
		x.send();
}

//executes sku blocking operation with the amazon order id
function AjaxSkuBlockingExecute(amazonOrderId){
	var wcfUrl = GetWcfServiceUrl() + '/SkuBlockingService.svc/BlockSku/' + amazonOrderId;
	$.ajax({
                    type: "GET",
                    url: wcfUrl,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processData: true,
                    success: function(result) {
						alert(result);
						if(result == "Blocking Operation succeeded"){
							HideButton('skuBlockButtonDivision');
						}
                        
                    },
                    error:  function(result) {
	                    console.log(result);
	                }
                });
}
// Saves options to chrome.storage
function save_options() {
  var autoCheckVal = document.getElementById('dnAutoCheck').checked;
  chrome.storage.sync.set({
    autoCheck: autoCheckVal
  }, function() {
    // Update status to let user know options were saved.
    var status = document.getElementById('dnOptionsStatus');
    status.textContent = 'Options saved.';
    setTimeout(function() {
      status.textContent = '';
    }, 750);
  });
}

// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restore_options() {
  // Use default value color = 'red' and likesColor = true.
  chrome.storage.sync.get({
    autoCheck: true
  }, function(items) {
    document.getElementById('dnAutoCheck').checked = items.autoCheck;
  });

}
document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('dnSaveOptions').addEventListener('click', save_options);



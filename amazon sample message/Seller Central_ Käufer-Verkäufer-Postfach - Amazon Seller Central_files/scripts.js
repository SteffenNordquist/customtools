

function isArray( obj ) {
    return Object.prototype.toString.call(obj) === "[object Array]";
}

function clickclear(thisfield, defaulttext) {
    thisfield.style.color = "#000000";
    if (thisfield.value == defaulttext) {
        thisfield.value = "";
    }
}

function clickrestore(thisfield, defaulttext) {
    if (thisfield.value == "") {
        thisfield.style.color = "#808080";
        thisfield.value = defaulttext;
    }
}

function getWidth(text)
{
    var spanElement = document.createElement("span");
    spanElement.style.whiteSpace = "nowrap";
    spanElement.innerHTML = text;
    document.body.appendChild(spanElement);
    var width = spanElement.offsetWidth;
    document.body.removeChild(spanElement);

    return width;
}

var _modalWindow = null;

function helppopup(url, toolbar)
{
    if(window.Event) window.top.captureEvents(Event.CLICK|Event.FOCUS);
    window.top.onclick = ignoreEvents;
    window.top.onfocus = focusHandler;

    var w = 725;
    var h = 550;

    if (toolbar == null) {toolbar = "no";}

    l = (screen.width - w) / 2;
    t = (screen.height - h) / 2;
    _modalWindow = window.open(url, null,
        "height="+h+",width="+w+",left="+l+",top="+t+
        ",status=no,toolbar="+toolbar+",menubar=no,location=no,resizable=yes,scrollbars=yes");
    _modalWindow.window.focus();
}

function ignoreEvents(e)
{
    if(_modalWindow)
    {
        if(!_modalWindow.closed)
        {
            return false;
        }
    }
    return true;
}

function focusHandler()
{
    if(_modalWindow)
    {
        if(!_modalWindow.closed)
        {
            _modalWindow.focus();
        }
        else
        {
            if(window.Event) window.top.releaseEvents(Event.CLICK|Event.FOCUS);
            window.top.onclick = "";
            window.top.onfocus = "";
        }
    }
}


function onDisplayExpandAdjustment(id){
  var overflowDiv = document.getElementById("outer_" + id);
  if (overflowDiv == null)
    return;
  var contentDiv = document.getElementById(id);
  var showMoreDiv = document.getElementById("expandContent");
  var showLessDiv = document.getElementById("collapseContent");

  var oHeight = overflowDiv.clientHeight;
  var cHeight = contentDiv.scrollHeight + 20;
  if (cHeight > oHeight){
    showMoreDiv.style.display = 'block';
    showLessDiv.style.display = 'none';
  } else {
    overflowDiv.style.height = cHeight + "px";
    showLessDiv.style.display = 'none';
    showMoreDiv.style.display = 'none';
  }
}

OROWidgets = (function($){
	
	//ORO popover manager
	var OROPopoverManager = function(selector, title, pageSize, moreDetails){
		amznJQ.onReady('popover', function(){
			$(selector).each(function(){
				new OROPopover(this, title, pageSize, moreDetails); 
			});
		});
	};
	
	//ORO popover
	var OROPopover = function(selector, title, pageSize, moreDetails){	
		var popover = this;

		popover.container = null;
		popover.selector = selector;
		popover.maxCharsInSellerNote = 100; // defaults to 100; as requested by SXD
		popover.maxPages = 10; // defaults to 10; as requested by SXD
		popover.hasMoreTokens = true; // has more tokens on the server side; assume true unless discover false
		
		popover.paginationSelector = ".pyop-pagination-page-number"; // pagination pages, 1, 2, 3...
		popover.paginationSelectorClass = "pyop-pagination-page-number"; // selector's class
		popover.sellerNoteFullSelector = ".pyop-seller-note-full"; // container for seller note full text
		popover.sellerNoteSelector = ".pyop-seller-note"; // seller-note short version
		popover.crNoteSelector = ".completed-refunds-note"; // container for refund explainition texts
		popover.moreSelector = ".pyop-show-seller-note-full"; // more button
		popover.etc = ".pyop-seller-note-etc"; // ...
		popover.explainSelector = ".completed-refunds-note-explain"; // question img
		popover.transactionsReplaceSelector = ".pyop-transactions-details-table"; // ajax replace new transactions
		popover.paginationAppendSelector = ".pyop-pagination-page-list"; // ajax append new pagination pages
		popover.prevSelector = ".pyop-pagination-page-prev"; // prev button
		popover.nextSelector = ".pyop-pagination-page-next"; // next button
		popover.errorMsgSelector = ".pyop-ajax-error-box"; // message box for ajax call errors
		popover.loadSelector = ".pyop-loader-overlay"; // on ajax loading
		
		var self = $(popover.selector);
		var text = $.trim(self.text());
		self.amazonPopoverTrigger({
			width: 1000,
			location: "centered",
			title: title,
			draggable: true,
			destination:"/gp/pyop/oro/order-reference-details-view.html?orderReferenceId="+ text + "&pageSize=" + pageSize + "&moreDetails="+moreDetails,
			onFilled: function( container, settings )
			{				
				// sets this popover's container
				popover.container = container;
				
				// toggle refund help explain and more seller note
				popover.bindRefundNotesPopover();
				popover.bindMoreSellerNotesPopover();
												
				// bind loading spinner
				popover.bindLoadSpinner();				
				
				if(container.find(popover.paginationSelector).length > 1){
					popover.bindPrevNext();	// bind prev/next button;
					popover.updatePrevNext(container.find(popover.paginationSelector + ".active"));
					popover.bindPaginationPages(container.find(popover.paginationSelector));// click each page number to get another set of transactions	
				}else{// hides prev/next if page <= 1
					container.find(popover.prevSelector).hide();
					container.find(popover.nextSelector).hide();
				}
			}
		});
	};
	


	OROPopover.prototype = {// prototypes
		
		bindRefundNotesPopover : function(){
			// toggle refund help explain
			var popover = this;
			var container = popover.container;
				

			jQuery(".tool-tip").each (function(){ 
 				var target = jQuery(this);
 				var message = "#"+target.attr("data-id");
				target.amazonPopoverTrigger({
					width: 600,
					align: "center",
					attached: true,
					showCloseButton: false,
					showOnHover: true,
					hoverShowDelay: 500,
					hoverHideDelay: 500,
					closeEventInclude:["MOUSE_LEAVE", "CLICK_OUTSIDE"],
					locationElement: target,
					localContent: message
				});
			});

			container.find(popover.explainSelector).amazonPopoverTrigger({
				width: 300,
				align: "center",
				//paddingLeft: 0,
				//paddingRight: 0,
				attached: true,
				showCloseButton: false,
				showOnHover: true,
				hoverShowDelay: 500,
				hoverHideDelay: 500,
				locationOffset: [30, 10],
				closeEventInclude:["MOUSE_LEAVE", "CLICK_OUTSIDE"],
				locationElement: container.find(popover.explainSelector),
				localContent: container.find(popover.crNoteSelector)
			});
		},
		
		bindMoreSellerNotesPopover : function(){
			// toggle for full seller notes
			var popover = this;
			var container = popover.container;
			
			var fullText = container.find(popover.sellerNoteSelector).text();
			if(fullText.length > popover.maxCharsInSellerNote){// default to 100 characters			
				// update truncated text
				container.find(popover.sellerNoteSelector).html(fullText.substring(0, popover.maxCharsInSellerNote));
				container.find(popover.etc).show(); // shows " ... "
				
				//update full text div and show
				container.find(popover.sellerNoteFullSelector).html(fullText);
				container.find(popover.moreSelector).show();

   


					
				//toggle more button
				container.find(popover.moreSelector).amazonPopoverTrigger({
					width: 500,
					align: "center",
					//paddingLeft: 0,
					//paddingRight: 0,
					attached: true,
					showCloseButton: false,
					showOnHover: true,
					hoverShowDelay: 500,
					hoverHideDelay: 500,
					locationOffset: [150, 10],
					closeEventInclude: ["MOUSE_LEAVE", "CLICK_OUTSIDE"],
					locationElement: container.find(popover.moreSelector),
					localContent: container.find(popover.sellerNoteFullSelector)
				});
			}
		},
		
		// bind loading spinner
		bindLoadSpinner : function(){
			var popover = this;
			var container = popover.container;
			
			container.find(popover.loadSelector).ajaxStart(function() {
				$(this).show();
		    }).ajaxStop(function(){
		    	$(this).hide();
		    });

		},
		
		bindPaginationPages : function (selector){
			var popover = this;
			var container = popover.container;
			
			selector.each(function(){
				var self = $(this);
				
				self.bind("click", function(){
					
					// if is current active; do nothing
					if(self.hasClass('active')){
						return;
					}
					
					var orderReferenceId = self.attr("orderReferenceId");
					var pageToken = self.attr("pageToken");
					var pageNumber = self.attr("pageNumber");
					var urlString = "/gp/pyop/oro/ajax-order-reference-txns.html";
					
					$.ajax({
						type:"POST",
						url:urlString,
						data:{"orderReferenceId":orderReferenceId, "pageToken":pageToken, "pageNumber":pageNumber},
						success: function(data){
							// hides error message
							container.find(popover.errorMsgSelector).hide();
							container.find(popover.transactionsReplaceSelector).replaceWith(data);
						},
						error: function(xhr, textStatus, errorThrown) {
							// displays a generic error message
							container.find(popover.errorMsgSelector).show();
							container.find(popover.transactionsReplaceSelector).html("");
						},
						complete: function(xhr, textStatus){
							// updates the css of new current page
							container.find(popover.paginationSelector + ".active").removeClass('active');
							self.addClass('active');
							
							//show/hide prev/next && updatePagination
							popover.updatePagination(self);
							popover.updatePrevNext(self);
						}
					});
				});
			});
		},
		
		//enable disable prev/next
		updatePrevNext : function(currentPage){
			var popover = this;
			var container = popover.container;
			
			
			if(currentPage.attr("pageNumber") != "1"){
				container.find(popover.prevSelector).removeAttr("disabled");
			}else{
				container.find(popover.prevSelector).attr("disabled", "disabled");
			}
			
			if(currentPage.attr("pageNumber") != container.find(popover.paginationSelector + ":last").attr("pageNumber")){
				container.find(popover.nextSelector).removeAttr("disabled");
			}else{
				container.find(popover.nextSelector).attr("disabled", "disabled");
			}
		},
		
		// bind prev/next buttons
		bindPrevNext : function(){
			var popover = this;
			var container = popover.container;
			
			container.find(popover.prevSelector).click(function(){
				var prevPage = container.find(popover.paginationSelector + '.active').prev();
				if(prevPage != null && prevPage.hasClass(popover.paginationSelectorClass)){
					prevPage.trigger("click");
				}
			});

			container.find(popover.nextSelector).click(function(){
				var nextPage = container.find(popover.paginationSelector + '.active').next();
				if(nextPage != null && nextPage.hasClass(popover.paginationSelectorClass)){
					nextPage.trigger("click");
				}
			});
		},
		
		// shows the 4 pages to the right of the current pagination page, the current page,
		// and the next 5 pages to the right of the current pagination page by default if available;
		updatePagination : function(currentPage){
			var popover = this;
			var container = popover.container;
			
			// if the current pagination page is reaching the last page; then get more tokens
			var currentNumber = parseInt(currentPage.attr('pageNumber'));
			var lastNumber = parseInt(container.find(popover.paginationSelector + ':last').attr('pageNumber'));
			if((lastNumber-currentNumber) < popover.maxPages){
				if(popover.hasMoreTokens){
					popover.getNewTokens();//else get new tokens
				}
			}

			// hide all elements under pyop-pagination-page-list
			container.find(popover.paginationAppendSelector + " li").hide();	
			
			// show left/right/left, until default 10 pages are visible
			var isLeft = true;
			var left = currentPage;
			var right = currentPage.next();
			for(var i = 0; i  <  popover.maxPages; i++){
				if(isLeft && left != null && left.hasClass(popover.paginationSelectorClass)){
					left.show();
					left = left.prev();
				}else if(right != null && right.hasClass(popover.paginationSelectorClass)){
					right.show();
					right = right.next();
				}else if(container.find(popover.paginationSelector).length > container.find(popover.paginationSelector + ":visible").length){
					i -= 1;
				}
				isLeft = !isLeft;
			}
		},
		
		// get more tokens from the server side if available
		getNewTokens : function(){//use the last page	
			var popover = this;
			var container = popover.container;
			
			if(!popover.hasMoreTokens){
				return;
			}

			var currentLastPage = container.find(popover.paginationSelector + ":last");
			var orderReferenceId = currentLastPage.attr("orderReferenceId");
			var pageToken = currentLastPage.attr("pageToken");
			var pageNumber = currentLastPage.attr("pageNumber");
			var totalLength = container.find(popover.paginationSelector).length;
			var urlString = "/gp/pyop/oro/ajax-order-reference-paginations.html";
			$.ajax({
				type:"POST",
				url:urlString,
				data: {"orderReferenceId":orderReferenceId, "pageToken":pageToken, "pageNumber":pageNumber},
				async: false,
				success: function(data){
					// hides error message
					container.find(popover.errorMsgSelector).hide();
					
					// adds to table with new paginations and get the new last page number
					container.find(popover.paginationAppendSelector).append(data);
					var newTotalLength = container.find(popover.paginationSelector).length;
					
					if(totalLength == newTotalLength){// no new tokens were added
						popover.hasMoreTokens = false; // does not have any more tokens
					}else{
						//binds the new pagination pages
						popover.bindPaginationPages(container.find(popover.paginationSelector + ':gt(' + (pageNumber - 1) + ')'));
					}
				},
				error: function(xhr, textStatus, errorThrown) {
					// displays generic error message
					container.find(popover.errorMsgSelector).show();
				}
			});
		}
	};
	
	return {OROPopoverManager:OROPopoverManager, OROPopover:OROPopover};// return the widget
})(jQuery);

amznJQ.declareAvailable('pyop-oro-popover');

//used for scraping the amazon order id of the current page
function GetAmazonOrderId(){
	var orderIdTag = $('a[href^="https://sellercentral.amazon.de/gp/orders-v2/details?ie=UTF8&orderID="]');
	var orderIdValue = orderIdTag.attr('href').replace("https://sellercentral.amazon.de/gp/orders-v2/details?ie=UTF8&orderID=","");
	return orderIdValue;
}
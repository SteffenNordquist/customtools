
//checking if we are in the right amazon page to work with
if(document.title == "Seller Central: Käufer-Verkäufer-Postfach - Amazon Seller Central")
{
	document.getElementById('setProductStockButtonDivision').addEventListener('click', SetProductStockToZero);
}

function SetProductStockToZero(){
	var wcfUrl = GetWcfServiceUrl() + 'ProductStockService.svc/SetProductStock?amazonOrderId=' + amazonOrderId + '&stock=0';
	$.ajax({
                    type: "GET",
                    url: wcfUrl,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processData: true,
                    success: function(result) {
						alert(result);
                        
                    },
                    error:  function(result) {
	                    alert(result);
	                }
                });
}
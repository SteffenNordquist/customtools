
//checking if we are in the right amazon page to work with
if(document.title == "Seller Central: Käufer-Verkäufer-Postfach - Amazon Seller Central")
{
	AjaxHasReOrdered();
}

//not in use currently
function HasReOrdered(){
		var amazonOrderId = GetAmazonOrderId();
		var x = new XMLHttpRequest();
		x.open('GET', GetWcfServiceUrl() + '/ReOrderingService.svc/HasReordered/' + amazonOrderId);//web service link
		x.onload = function() {
			var hasReOrderedResponse = "";
			hasReOrderedResponse = x.responseText;//get response from webservice
			hasReOrderedResponse = hasReOrderedResponse.replace('<boolean xmlns="http://schemas.microsoft.com/2003/10/Serialization/">',"");
			hasReOrderedResponse = hasReOrderedResponse.replace('</boolean>',"");

			if(hasReOrderedResponse == false || hasReOrderedResponse == "false"){
				document.getElementById('reOrderButton').addEventListener('click', SetupReOrdering);
			}
			else if(hasReOrderedResponse == true || hasReOrderedResponse == "true"){
				HideButton('reOrderButtonDivision');
			}
		};
		x.send();

		
}

//checks if an amazon order id has reordered
//adds event listener for reordering button once it has not reordered
function AjaxHasReOrdered(){
		var amazonOrderId = GetAmazonOrderId();
		var wcfUrl = GetWcfServiceUrl() + '/ReOrderingService.svc/HasReordered/' + amazonOrderId;
		$.ajax({
                    type: "GET",
                    url: wcfUrl,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processData: true,
                    success: function(result) {
						if(result == false || result == "false"){
							document.getElementById('reOrderButton').addEventListener('click', SetupReOrdering);
						}
						else if(result == true || result == "true"){
							HideButton('reOrderButtonDivision');
						}
                    },
                    error:  function(result) {
	                    console.log(result);
	                }
                });
}

//triggered if the added event listener for reordering button is clicked
function SetupReOrdering(){
	
	var amazonOrderId = GetAmazonOrderId();
	

	if(amazonOrderId != ""){
		if (confirm('This process cant be undone. Are you sure you want to reorder?')) 
		{
			//ReOrderExecute(amazonOrderId);
			AjaxReOrderExecute(amazonOrderId);
		}
	}
	else{
		alert("AmazonOrderId not found!");
	}
}

//not in use currently
function ReOrderExecute(amazonOrderId){
		var x = new XMLHttpRequest();
		x.open('GET', GetWcfServiceUrl() + '/ReOrderingService.svc/ReOrder/' + amazonOrderId);//web service link
		x.onload = function() {
			var reOrderResponse = "";
			reOrderResponse = x.responseText;//get response from webservice
			reOrderResponse = reOrderResponse.replace('<string xmlns="http://schemas.microsoft.com/2003/10/Serialization/">',"");
			reOrderResponse = reOrderResponse.replace('</string>',"");
			alert(reOrderResponse);
			if(reOrderResponse == "Operation success"){
				document.getElementById('reOrderButton').disabled = true;
				document.getElementById('reOrderButton').setAttribute("style","padding:0;margin:0;width:100%;height:55px;background-color','#E4F1FE");
			}	
		};
		x.send();
}

//executes reordering with amazon order id
function AjaxReOrderExecute(amazonOrderId){
		var wcfUrl = GetWcfServiceUrl() + '/ReOrderingService.svc/ReOrder/' + amazonOrderId;
		$.ajax({
                    type: "GET",
                    url: wcfUrl,
                    contentType: "application/json; charset=utf-8",
                    dataType: "text",
                    processData: true,
                    success: function(result) {
                    	alert(result);
						if(result.toString().indexOf("success") != -1){
							HideButton('reOrderButtonDivision');
						}
                    },
                    error: function(xhr, status, error) {
					  var err = JSON.parse(xhr.responseText);
					  alert(err.Message);
					}
                });
}

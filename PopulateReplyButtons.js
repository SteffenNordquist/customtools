
//This js file is used to generate reply buttons

//checking if we are in the right amazon page to work with
if(document.title == "Seller Central: Käufer-Verkäufer-Postfach - Amazon Seller Central")
{
	AjaxGetAgentCustomMessageNames(GetAmazonOrderId());
}

//not in use currently
function GetAgentCustomMessageNames(amazonOrderId){
		//amazonEmail = "berliner.versandwerk@gmail.com";
		var x = new XMLHttpRequest();
		x.open('GET', GetWcfServiceUrl() + '/AgentCustomReplyMsgService.svc/GetAgentCustomMessages/' + amazonOrderId);//web service link
		x.onload = function() {
			var namesList = JSON.parse(x.responseText);
			namesList.forEach(function(name){
				console.log(name);
				MakeButton(name);
			});

		};
		x.send();
}

//gets custom messages for an agent
//make buttons out of the custom messages
function AjaxGetAgentCustomMessageNames(amazonOrderId){
	var wcfUrl = GetWcfServiceUrl() + '/AgentCustomReplyMsgService.svc/GetAgentCustomMessages/' + amazonOrderId;
	$.ajax({
                    type: "GET",
                    url: wcfUrl,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    processData: true,
                    success: function(result) {
						result.forEach(function(name){
							MakeButton(name);
						});
                        
                    },
                    error:  function(result) {
	                    console.log(result);
	                }
                });
}

//button creator
function MakeButton(messageName){
	var container = document.getElementById('customerServiceToolContainer');
			
			var buttonTextValue = messageName;

			var replyButtonDivision = document.createElement("div");
			replyButtonDivision.setAttribute("style","width:95%;height:auto;padding-top:0px;padding-bottom:5px;");
			replyButtonDivision.id = "CustomReplyButtons";
				var replyButton = document.createElement("button");
				replyButton.id = messageName;
				replyButton.setAttribute("style","padding:10px 0px 10px 20px;margin:0;width:100%;height:auto;background-color:#87D37C;text-align:left;");
					var replybuttonValue = document.createTextNode(buttonTextValue);
					replyButton.appendChild(replybuttonValue);
						
			replyButtonDivision.appendChild(replyButton);
				
			container.appendChild(replyButtonDivision);

			replyButton.addEventListener('click', function(){
    			SetupReply(messageName);
			});
}




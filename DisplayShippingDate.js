
//checking if we are in the right amazon page to work with
if(document.title == "Seller Central: Käufer-Verkäufer-Postfach - Amazon Seller Central"){
		var amazonOrderId = GetAmazonOrderId();//function located in OrderIdParser.js
		if(amazonOrderId != ""){
			//wcf service that will return shipped date of the amazon order id
			var wcfUrl = GetWcfServiceUrl() + "/ShippedDateService.svc/GetShippingDate/" + amazonOrderId;
			//invokes the function located in ExternalServices.js
			InvokeGetServiceAndDisplay(wcfUrl, "ShipDateValueHere");
		}
}

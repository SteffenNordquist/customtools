# README #

This tool is composed of .json and .js files. This tool mainly works on amazon messages pages. Amazon specific message page has an amazon order id and this tool will create html elements (button, etc...) to work with. Generated html elements are buttons to execute and information of the current page's amazon order id such as shipping date information, cheapest supplier, and etc. It is currently deployed in the agents servers we have.



### How do I get set up? ###

* Add this extension to chrome ( tutorial : http://www.cnet.com/how-to/how-to-install-chrome-extensions-manually/ ) locate the dnamazoncustomerservicetool to install

   ![2016-02-11_1754.png](https://bitbucket.org/repo/4rKnG6/images/1517413936-2016-02-11_1754.png)

* This extension wont work if you dont 'Load unsafe scripts'. Once the shield icon is present, click it and 'Load unsafe scripts'
* Dependencies - wcf services [WCF SERICES REPO](https://bitbucket.org/SteffenNordquist/dn-buchkoenig-wcfservices)
* How to tests. Visit specific amazon message page in one of the agent server to see displayed information by the customer service tool
* Deployment Instructions - Deploy in each agent server C:/DevNetworks/DNAmazonCustomerServiceTool. Follow adding chrome extension tutorial

### Who do I talk to? ###

* Wylan Osorio